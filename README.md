# README #

Wellcome to project Barley

### What is this repository for? ###

* This repository contains the R version of http://ico2s.org/servers/enrichnet.html
* It can be modified for plant networks following the details in [HowToConvertToPlantsData.txt](https://bitbucket.org/natk/enrichnet-plants/src/master/HowToConvertToPlantsData.txt)

* [Use Markdown](https://bitbucket.org/tutorials/markdowndemo) to improve this file

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Natalio Krasnogor: natalio_dot_krasnogor_at_newcastle.ac.uk
* Katy Brown:  catherine_dot_m_dot_brown_at_durham.ac.uk
* Peter Etchells: peter_dot_etchells_at_durham.ac.uk